package com.homework.service;

import com.homework.model.Employee;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class SearchEngineTest {
    private SearchEngine searchEngine;

    @Before
    public void setUp() {
        searchEngine = new SearchEngine();
    }

    @Test
    public void extractOnlyAllowedParameters() {
        Map<String, List<String>> map = new HashMap<>();
        map.put("name", Collections.singletonList("dummy"));
        map.put("surname", Collections.singletonList("dummy"));
        map.put("test", Collections.singletonList("dummy"));

        Map<String, List<String>> extractedMap = searchEngine.extractOnlyAllowedParameters(map);

        assertEquals(extractedMap.size(), 2);
        assertNotNull(extractedMap.get("name"));
        assertNotNull(extractedMap.get("surname"));
    }

    @Test
    public void search_basic() {
        Map<String, List<String>> map = new HashMap<>();
        map.put("name", Collections.singletonList("name1"));

        Set<Employee> search = searchEngine.search(this::customFindEmployees, map);

        assertEquals(search.size(), 1);
        assertTrue(customFindEmployees().stream().anyMatch(x -> x.getName().equals("name1")));
    }

    @Test
    public void search_multiple_parameters() {
        Map<String, List<String>> map = new HashMap<>();
        map.put("name", List.of("name1", "name2"));

        Set<Employee> search = searchEngine.search(this::customFindEmployees, map);

        assertEquals(search.size(), 2);
        assertTrue(customFindEmployees().stream().anyMatch(x -> x.getName().equals("name1")));
        assertTrue(customFindEmployees().stream().anyMatch(x -> x.getName().equals("name2")));
    }

    @Test
    public void search_multiple_with_additional_parameter() {
        Map<String, List<String>> map = new HashMap<>();
        map.put("name", List.of("name1", "name2"));
        map.put("salary", List.of("1"));

        Set<Employee> search = searchEngine.search(this::customFindEmployees, map);

        assertEquals(search.size(), 1);
        assertTrue(customFindEmployees().stream().anyMatch(x -> x.getName().equals("name2")));
    }
    
    
    private List<Employee> customFindEmployees() {
        return List.of(
                new Employee("name1", "surname1", 1, 1),
                new Employee("name2", "surname2", 1, 2),
                new Employee("name3", "surname3", 1, 3),
                new Employee("name4", "surname4", 2, 4),
                new Employee("test5", "surname5", 2, 5)
        );
    }
}
