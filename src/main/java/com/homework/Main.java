package com.homework;

import com.blade.Blade;
import com.github.javafaker.Faker;
import com.homework.model.Employee;
import com.homework.service.EmployeeApi;
import com.homework.service.EmployeeService;
import com.homework.service.SearchEngine;

public class Main {
    private static final String SAMPLE = "-load.samples";
    private static final String LOAD_SAMPLES = "true";

    public static void main(String... args) {
        EmployeeService employeeService = EmployeeService.getInstance();
        SearchEngine searchEngine = new SearchEngine();
        EmployeeApi api = new EmployeeApi(employeeService, searchEngine);

        if (args.length > 1 && args[0].equals(SAMPLE) && args[1].equals(LOAD_SAMPLES))
            loadSampleData(employeeService);

        Blade.of()
                .get("/employee", api::search)
                .get("/employee/:id", api::find)
                .post("/employee", api::create)
                .put("/employee/:id", api::update)
                .delete("/employee/:id", api::delete)
                .listen(8080)
                .start();
    }

    private static void loadSampleData(EmployeeService service) {
        Faker faker = new Faker();
        for (int i = 0; i < 100; i++) {
            String price = faker.commerce().price();
            service.save(new Employee(faker.name().firstName(), faker.name().lastName(), i % 10, Integer.parseInt(price.substring(0, price.indexOf(".")))));
        }
    }
}
