package com.homework.service;

import com.homework.model.Employee;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.Serializable;
import java.util.List;

public class EmployeeService {
    private SessionFactory sessionFactory;
    private static EmployeeService employeeService;

    public static EmployeeService getInstance() {
        if (employeeService == null)
            employeeService = new EmployeeService();

        return employeeService;
    }

    private EmployeeService() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    public Employee find(Integer id) {
        Session session = sessionFactory.openSession();
        Employee employee = null;

        try {
            session.beginTransaction();
            employee = session.find(Employee.class, id);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (session.getTransaction() != null)
                session.getTransaction().rollback();

        } finally {
            session.close();
        }

        return employee;
    }

    public List<Employee> findAll() {
        Session session = sessionFactory.openSession();
        List<Employee> list = null;

        try {
            session.beginTransaction();
            list = session.createQuery("FROM Employee", Employee.class).list();
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (session.getTransaction() != null)
                session.getTransaction().rollback();

        } finally {
            session.close();
        }

        return list;
    }

    public Integer save(Employee employee) {
        Session session = sessionFactory.openSession();
        Employee object = new Employee(employee);
        Integer id = -1;

        try {
            session.beginTransaction();
            id = (Integer) session.save(object);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (session.getTransaction() != null)
                session.getTransaction().rollback();

        } finally {
            session.close();
        }

        return id;
    }

    public void update(Employee employee, Integer id) {
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();
            Employee foundEmployee = session.get(Employee.class, id);
            foundEmployee.set(employee);
            session.update(foundEmployee);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (session.getTransaction() != null)
                session.getTransaction().rollback();

        } finally {
            session.close();
        }
    }

    public void delete(Integer id) {
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();
            Employee employee = session.get(Employee.class, id);
            session.delete(employee);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            if (session.getTransaction() != null)
                session.getTransaction().rollback();

        } finally {
            session.close();
        }
    }
}
