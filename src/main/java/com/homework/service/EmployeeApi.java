package com.homework.service;

import com.blade.mvc.RouteContext;
import com.homework.model.Employee;
import com.homework.model.SaveResult;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class EmployeeApi {
    private EmployeeService service;
    private SearchEngine searchEngine;

    public EmployeeApi(EmployeeService employeeService, SearchEngine searchEngine) {
        this.service = employeeService;
        this.searchEngine = searchEngine;
    }

    public void search(RouteContext ctx) {
        Map<String, List<String>> parameters = searchEngine.extractOnlyAllowedParameters(ctx.parameters());
        Set<Employee> result = searchEngine.search(this.service::findAll, parameters);
        ctx.response().json(result);
    }

    public void find(RouteContext ctx) {
        Employee employee = this.service.find(ctx.pathInt(":id"));

        if (employee == null)
            ctx.response().status(404);
        else
            ctx.response().json(employee);
    }

    public void create(RouteContext ctx) {
        Integer save = service.save(ctx.request().bindWithBody(Employee.class));

        if (save == -1)
            ctx.response().status(500);
        else
            ctx.response().json(new SaveResult(save));
    }

    public void update(RouteContext ctx) {
        Employee employee = this.service.find(ctx.pathInt(":id"));

        if (employee != null)
            service.update(ctx.request().bindWithBody(Employee.class), ctx.pathInt(":id"));
        else
            ctx.response().status(404);
    }

    public void delete(RouteContext ctx) {
        Employee employee = this.service.find(ctx.pathInt(":id"));

        if (employee != null)
            this.service.delete(ctx.pathInt(":id"));
        else
            ctx.response().status(404);
    }
}
