package com.homework.service;

import com.homework.model.Employee;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class SearchEngine {
    public Map<String, List<String>> extractOnlyAllowedParameters(Map<String, List<String>> map) {
        return map.entrySet()
                .stream()
                .filter(SearchEngine::isWithinAllowedKeys)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public Set<Employee> search(Supplier<List<Employee>> getEmployees, Map<String, List<String>> parameters) {
        return getEmployees.get()
                .parallelStream()
                .filter(employee -> matchWithGivenParameters(employee, parameters))
                .collect(Collectors.toSet());
    }

    private boolean matchWithGivenParameters(Employee employee, Map<String, List<String>> parameters) {
        return parameters.entrySet()
                .stream()
                .allMatch(parameter -> matchWithGivenParameters(employee, parameter));
    }

    private boolean matchWithGivenParameters(Employee employee, Map.Entry<String, List<String>> parameter) {
        String employeeVar = employee.getFilter().get(parameter.getKey());

        return parameter.getValue().stream().anyMatch(x -> x.equals(employeeVar));
    }

    private static boolean isWithinAllowedKeys(Map.Entry<String, List<String>> parameter) {
        return Employee.getPossibleKeys().contains(parameter.getKey());
    }
}
