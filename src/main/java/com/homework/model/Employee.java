package com.homework.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.HashMap;
import java.util.Set;

@Entity
@NoArgsConstructor
@Data
public class Employee {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String surname;
    private Integer grade;
    private Integer salary;

    public Employee(String name, String surname, Integer grade, Integer salary) {
        this.name = name;
        this.surname = surname;
        this.grade = grade;
        this.salary = salary;
    }

    public Employee(Employee employee) {
        this.name = employee.name;
        this.surname = employee.surname;
        this.grade = employee.grade;
        this.salary = employee.salary;
    }

    public void set(Employee employee) {
        this.name = employee.name;
        this.surname = employee.surname;
        this.grade = employee.grade;
        this.salary = employee.salary;
    }

    public HashMap<String, String> getFilter() {
        HashMap<String, String> map = new HashMap<>();

        map.put("id", String.valueOf(getId()));
        map.put("name", getName());
        map.put("surname", getSurname());
        map.put("grade", String.valueOf(getGrade()));
        map.put("salary", String.valueOf(getSalary()));

        return map;
    }

    public static Set<String> getPossibleKeys() {
        return Set.of("id", "name", "surname", "grade", "salary");
    }
}
