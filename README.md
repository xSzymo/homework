### How to run

Install jdk12 and maven 3.6

-----------------------------------------------------------------------------------------------------------------------
```
git clone https://gitlab.com/xSzymo/homework.git
mvn clean install
mvn exec:java -Dload.samples=true
```
Default access on ``` http://localhost:8080/ ```<br>


### Endpoints

-----------------------------------------------------------------------------------------------------------------------
- host:8080/employee - POST
    - Example request to create employee : 
```
    localhost:8080/employee
    {
        "name": "Name",
        "surname": "Surname",
        "grade": 1,
        "salary": 1500
    }
```
        
- host:8080/employee/{id} - GET
    - Example request to get employee with specific id : 
```
    localhost:8080/employee/1
    {
    
    }
```
- host:8080/employee/{id} - PUT
    - Example request to update employee : 
```
    localhost:8080/employee/1
    {
        "name": "NewName",
        "surname": "Surname",
        "grade": 1,
        "salary": 1500
    }
```
- host:8080/employee/{id} - DELETE
    - Example request to delete employee : 
```
    localhost:8080/employee/1
    {
    
    }
```

- host:8080/employee/params - GET
    - Simple engine to search employee by his fields
    - Use any of "id", "name", "surname", "grade", "salary" as request parameters
    - Example request to get all employees from first grade and with name Jack and Karen: 
```
    localhost:8080/employee?grade=1&name=Jack&name=Karen
    {
    
    }
```

### Frameworks :
- Blade
- Hibernate
- H2
- Lombok
- JUnit
- JavaFaker
  <br><br>
